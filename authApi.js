const express = require('express');
const router = express.Router();
const passport = require('passport')
const userModel = require('./models/user')
const key = require('./config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

require('./auth/passGoogle')

router.get("/auth/google",
    passport.authenticate('google', { scope: ['email', 'profile'] }))


router.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: 'http://localhost:3000', session: false }),
    function (req, res) {
        let googleUser = req.user._json
        let googleEmail = googleUser.email

        userModel.findOne(
            { email: googleEmail },
            function (_, user) {
                let payload
                if (!user) {
                    console.log("none, creating")
                    console.log(googleUser)
                    let { name, given_name, family_name, picture, locale } = googleUser
                    let password = Math.random().toString(36);
                    bcrypt.hash(password, saltRounds, function (err, hash) {
                        let newHashedPassword = hash
                        let newAccount = {
                            picture,
                            username: name,
                            password: newHashedPassword,
                            email: googleEmail,
                            firstName: given_name,
                            lastName: family_name,
                            country: locale
                        }
                        console.log(newAccount)
                        new userModel(newAccount).save();
                        payload = { //what will be saved as local token
                            email: googleEmail,
                            username: name,
                            password: newHashedPassword,
                            picture,
                            favorites: []
                        };
                    });
                } else { // there is a user matching
                    payload = { //what will be saved as local token
                        email: user.email,
                        username: user.username,
                        password: user.password,
                        picture: user.picture,
                        favorites: user.favorites
                    };
                }
                const options = { expiresIn: 2592000 };
                jwt.sign(payload, key.secretOrKey, options, (err, token) => {
                    if (err) {
                        res.redirect('http://localhost:3000/')
                    } else {
                        res.redirect('http://localhost:3000/login/token/' + token)
                    }
                });
            }
        )

        // res.redirect('http://localhost:3000/login') //redirect to a url that takes in a token, THEN to /cities
    }
)

module.exports = router
