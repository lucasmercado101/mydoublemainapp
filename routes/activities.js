const express = require('express');
const router = express.Router();
const activitiesModel = require('../models/activity');

router.get('/cities/activities', (req, res) => {
  activitiesModel
    .find()
    .then(datos => res.send(datos))
    .catch(error => console.log(error));
});

module.exports = router;
