const express = require('express');
const router = express.Router();
const userModel = require('../models/user');

router.post('/users/favorites', (req, res) => {
  let email = req.body.userData.email
  userModel.findOne(
    { email, favorites: req.body.cityName }, function (err, userFavsList) {
      let cityNotInFavs = userFavsList === null
      if (cityNotInFavs) {
        userModel.updateOne(
          { email },
          { $push: { favorites: req.body.cityName } },
          function (_, _) {
            return res.status(200).send();
          }
        )
      }
    }
  )
});

router.delete('/users/favorites', (req, res) => {
  userModel.updateOne(
    { email: req.body.userData.email },
    { $pull: { favorites: req.body.cityName } },
    function (_, _) {
      return res.status(200).send();
    }
  )
})

module.exports = router;
