const express = require('express');
const passport = require('../passport');
const router = express.Router();

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    userModel
      .findOne({ _id: req.user.id })
      .then(user => {
        console.log('valid');
        res.json(user);
      })
      .catch(err => res.status(404).json({ error: 'User does not exist!' }));
  }
);

module.exports = router;
