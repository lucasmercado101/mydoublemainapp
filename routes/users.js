const express = require('express');
const router = express.Router();
const userModel = require('../models/user');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;

router.post('/users', [check('email').isEmail()], (req, res) => {
  const errors = validationResult(req);
  let thereAreErrors = !errors.isEmpty()

  if (thereAreErrors) {
    console.log('something is invalid, did not post');
    return res.status(422).json({ errors: errors.array() });
  }
  userModel.findOne({ email: req.body.email }).then(resp => {
    exists = resp !== null;
    if (!exists) {

      bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
        req.body.password = hash
        console.log(req.body.password);
        console.log('creating');
        let user = new userModel(req.body);
        user
          .save()
          .catch(err => console.log(err))
          .then(data => res.send(data));
        res.status(200).send()
      });
    } else {
      res.status(401).send()
      console.log('already exists');
      return;
    }
  });
});

module.exports = router;
