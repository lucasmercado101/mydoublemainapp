const express = require('express');
const router = express.Router();
const passport = require('passport');
const citiesModel = require('../models/cities');

router.get(
  '/cities',
  passport.authenticate('jwt', { session: false }),
  (_, res) => {
    citiesModel
      .find()
      .then(datos => res.send(datos))
      .catch(error => console.log(error));
  }
);

module.exports = router;
