const key = require('../config.json');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const userModel = require('../models/user');
const bcrypt = require('bcrypt');
const { check, validationResult } = require('express-validator');

router.post('/users/login', [check('email').isEmail()], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log('something is invalid, did not post');
    return res.status(422).json({ errors: errors.array() });
  }
  userModel.findOne({ email: req.body.email }).then(resp => {
    let hashedPassword = resp.password
    bcrypt.compare(req.body.password, hashedPassword, function (_, passwordsCoincide) {
      if (passwordsCoincide || (req.body.password === hashedPassword)) {
        console.log("here")
        const payload = { //what will be saved as local token
          id: resp._id,
          email: resp.email,
          username: resp.username,
          password: resp.password,
          picture: resp.picture,
          favorites: resp.favorites
        };
        const options = { expiresIn: 2592000 };
        jwt.sign(payload, key.secretOrKey, options, (err, token) => {
          console.log("here also")
          if (err) {
            res.json({
              success: false,
              token: 'There was an error'
            });
          } else {
            res.json({
              success: true,
              token: token
            });
          }
        });
      }
    })
  }
  )


  // userModel.findOne({ email: req.body.email, password: req.body.password }).then(resp => {
  //   if (resp === null) {
  //     console.log('null');
  //     return;
  //   }
  //   console.log(resp);

  //   const payload = { //what will be saved as local token
  //     id: resp._id,
  //     email: resp.email,
  //     username: resp.username,
  //     password: resp.password,
  //     picture: resp.picture,
  //     favorites: resp.favorites
  //   };
  //   const options = { expiresIn: 2592000 };
  //   jwt.sign(payload, key.secretOrKey, options, (err, token) => {
  //     if (err) {
  //       res.json({
  //         success: false,
  //         token: 'There was an error'
  //       });
  //     } else {
  //       res.json({
  //         success: true,
  //         token: token
  //       });
  //     }
  //   });
  // });
});

module.exports = router