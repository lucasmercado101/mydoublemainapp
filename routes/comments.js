const express = require('express');
const router = express.Router();
const commentSection = require('../models/commentSection');

router.get('/itineraries/comments/:cityName', (req, res) => {
    commentSection
        .find({ city: req.params.cityName })
        .then(datos => res.send(datos))
        .catch(error => console.log(error));
});


router.delete('/itineraries/comments/:cityName', (req, res) => {
    let cityName = req.params.cityName
    let _id = req.body._id

    commentSection.update(
        { "city": cityName },
        { "$pull": { "comments": { "_id": _id } } },
        err => {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send();
                console.log("deleted: ", req.body)
            }
        }
    );
});

router.put('/itineraries/comments/:cityName', (req, res) => {
    console.log(req.body)
    let cityName = req.params.cityName
    let _id = req.body.data.original._id
    let editedComment = req.body.data.edit

    commentSection.update(
        { "city": cityName, "comments._id": _id },
        { "$set": { "comments.$.comment": editedComment } },
        err => {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send();
                console.log("updated: ", req.body)
            }
        }
    );
}
)


router.post('/itineraries/comments/:cityName', (req, res) => {
    let cityName = req.params.cityName
    commentSection.update(
        { "city": cityName },
        { "$push": { "comments": req.body } },
        err => {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send();
                console.log("added: ", req.body)
            }
        }
    );
});


module.exports = router;
