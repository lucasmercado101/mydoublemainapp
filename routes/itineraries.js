const express = require('express');
const router = express.Router();
const itineraryModel = require('../models/itinerary');

router.get('/itineraries/:cityName', (req, res) => {
  itineraryModel
    .find({ city: req.params.cityName })
    .then(datos => res.send(datos))
    .catch(error => console.log(error));
});

module.exports = router;
