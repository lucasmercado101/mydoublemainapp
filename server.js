const url =
  'mongodb+srv://Lucas101:mixing101@cluster0-jny0c.mongodb.net/cities?retryWrites=true&w=majority';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const passport = require('./passport');

// routes
const authapi = require('./authApi');
const citiesApi = require('./routes/cities');
const usersApi = require('./routes/users');
const activitiesApi = require('./routes/activities');
const itineraryCities = require('./routes/itineraries');
const landing = require('./routes/landing');
const updateFavs = require('./routes/userFavs');
const comments = require('./routes/comments');
const usersLogin = require('./routes/usersLogin');

app.use(passport.initialize());
app.use(bodyParser());
app.use(cors());
app.use('/uploads', express.static('uploads'))

mongoose.connect(url, { useNewUrlParser: true });

route = path => { app.use('/api', path); };

route(authapi);
route(citiesApi);
route(usersApi);
route(activitiesApi);
route(itineraryCities);
route(comments);
route(updateFavs);
route(usersLogin);
app.use('', landing);

var port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server running on port ${port}`));
