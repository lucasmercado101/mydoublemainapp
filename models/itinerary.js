const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItinerarySchema = new Schema({
  title: String,
  profilePic: String,
  rating: Number,
  duration: Number,
  price: Number,
  hashtag: Array
});

module.exports = mongoose.model('itinerary', ItinerarySchema);
