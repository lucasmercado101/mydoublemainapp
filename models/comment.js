const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
  user: String,
  comment: String
})
module.exports = mongoose.model('comment', commentSchema);
