const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
  user: String,
  comment: String
})

var commentSectionSchema = new Schema({
  city: String,
  comments: [commentSchema]
});

module.exports = mongoose.model('comments', commentSectionSchema);