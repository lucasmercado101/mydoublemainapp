import React, { Component } from 'react'
import Navbar from "react-bootstrap/Navbar"
import UserButton from "../src/components/UserButton"
import MenuButton from "../src/components/MenuButton"
import Homebutton from "../src/components/HomeButton"
import test from "../Images/test-image2.png"
import Paris from "../Images/Paris.jpg"

var cities

export default class Cities extends Component {
    constructor(props){
        super(props)
        this.state = {
        citiesAll: []
        }

    }

    async componentDidMount(){
        cities = await fetch("http://localhost:5000/api/cities")
        .then((data) => data.json());
        console.log(cities)
        this.setState({citiesAll: cities})
    }

    render() {
        return (
            <div>
              <Navbar className="main-nav justify-content-between">
                <UserButton />
                <MenuButton />
              </Navbar>
                <ul className="citiesImages">
                    {this.state.citiesAll.map((city)=> {
                        return <li id={city.name}>
                            <div className="city">
                                <span>
                                {city.name}
                                </span>
                                <img src={Paris}></img>
                            </div>
                        </li>
                    })}
                </ul>
                <footer>
                    <Homebutton />
                </footer>
            </div>
        )
    }
}
