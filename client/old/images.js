const express = require('express');
const router = express.Router();
const multer = require('multer')

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(":", "_").replace(":", "_") + " " + file.originalname);
  }
})

// const upload = multer({storage})
const upload = multer({storage}).single('picture')
// const upload = multer({dest: "uploads/"})



// router.post('/users/image', upload.single('picture'), (req, res) => {
router.post('/users/image', (req, res) => {
//   console.log(req.file)
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
    return res.status(200).send(req.file)

    })
});

module.exports = router