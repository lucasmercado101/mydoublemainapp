import React, { Component } from 'react';
import cityData from './Actions/itineraryActions';
import getComms from './Actions/getCommentsAction';
import allCities from './Actions/actionsCity';
import { connect } from 'react-redux';
import Activity from './Activity';
import { Link } from 'react-router-dom';
import LikeButton from './like';
import BottomLinks from './BackButtons';
import Comment from './comment';
import axios from 'axios';
import $ from 'jquery'

const mapStateToProps = state => {
  console.log(state);
  return {
    user: state.userData.userObject,
    cities: state.allCities.cities,
    comments: state.comments.commentObject,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    cityData: data => dispatch(cityData(data)),
    allCities: () => dispatch(allCities()),
    getComms: city => dispatch(getComms(city)),
  };
};

class Itinerary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cityData: [],
      activities: [],
      commentObject: [],
      allcomments: [],
      commentToAdd: ""
    };
    this.reloadComments = this.reloadComments.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  async reloadComments() {
    $('.lds-ring').addClass('on-comment')
    $('.comment').hide()

    await this.props.getComms(this.props.match.params.cityName)
    var commentObject = this.props.comments

    let thereAreComments = commentObject.length !== 0

    if (thereAreComments) {
      this.setState({
        allcomments: commentObject[0]["comments"]
      })
    }

    $('.lds-ring').removeClass('on-comment')
    $('.comment').show()
    $('.new-comment').show()
  }

  handleChange(e) {
    this.setState({ commentToAdd: e.target.value });
    console.log(this.state.commentToAdd)
  }

  async componentDidMount() {
    await this.props.allCities()
    for (let i in this.props.cities) {
      // console.log(this.props.cities[i].name)
      if (this.props.cities[i].name == this.props.match.params.cityName) {
        console.log(this.props.cities[i])
        this.setState({
          cityData: [this.props.cities[i]],
        })
      }
    }
    document.title = this.props.match.params.cityName
    console.log(this.state.currentCity)
    console.log("user stuff: ", this.props.user)

    this.reloadComments()

    $('.comment-section').slideDown(200)
    $(".post-comment").click(async function () {
      let commentToAddIsNotEmpty = this.state.commentToAdd !== ""

      if (commentToAddIsNotEmpty) {
        let newComment = {
          'user': this.props.user['username'],
          'comment': this.state.commentToAdd
        }

        await axios.post("http://localhost:5000/api/itineraries/comments/" + this.props.match.params.cityName, newComment)
        this.reloadComments()
      }
    }.bind(this))

  }

  render() {
    return (
      <div
        id='city-itineraries'
        style={{ display: 'flex', flexDirection: 'column' }}
      >
        {this.state.cityData.map(city => {
          return (
            <div id='city-data'>
              <header id='header-image'>
                <span id='city-title'>{city.name}</span>
                <img
                  src={city.url}
                  alt={city.name}
                ></img>
              </header>
            </div>
          );
        })}
        <p id='available-text' style={{ margin: '10px 10px' }}>
          Available Itineraries:
        </p>
        <Activity />
        {this.state.cityData.map(city => {
          return <LikeButton city={city.name} />;
        })}

        <div className="comment-section" style={{ display: 'none' }}>
          <p>Comments: </p>
          <div className="all-comments">
            <div class="lds-ring on"><div></div><div></div><div></div><div></div></div>
            {this.state.allcomments.map((obj, i) => {
              return <Comment reloadComments={this.reloadComments} index={i} city={this.props.match.params.cityName} comment={obj} />
            })}
          </div>
          <div className="new-comment" style={{ display: "none" }}>
            <input
              type="text" id="add-comment"
              value={this.state.commentToAdd}
              name='country'
              id='country'
              onChange={this.handleChange}
              placeholder="Your comment..."
            />
            <span className=" post-comment glyphicon glyphicon-triangle-right"></span>
          </div>
        </div>
        <footer>
          <Link to={'/cities'} style={{ alignSelf: 'center' }}>
            <span id='another-city'>choose another city...</span>
          </Link>
          <BottomLinks to="/cities" />
        </footer>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Itinerary);
