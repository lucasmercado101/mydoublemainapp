import React, { Component } from 'react'


export default class CityFilter extends Component {
    constructor(props){
        super(props)
        this.state = {
            cities: [],
            citiesFilter: []}
    }

    filterCities = (e) => {
        let filteredCities = this.state.cities        
        filteredCities = filteredCities.filter((city) => {
          let cityFiltered = city.name.toLowerCase() + "  " + city.country.toLowerCase()
          return cityFiltered.indexOf(
            e.target.value.toLowerCase()) !== -1
        });
        this.setState({
          citiesFilter: filteredCities
        })
        
      }

    async componentDidMount(){
        var citiesResponse = await fetch("http://localhost:5000/api/cities")
        .then((data) => data.json());
        console.log(citiesResponse)
        this.setState({citiesFilter: citiesResponse,
        cities: citiesResponse})
    }

        
    render() {

        return (
            <div>
                <label htmlFor="filter">Filter by city: </label>
                <input type="text" id="filter"  
                onChange={this.filterCities.bind(this)}/>
                <ul>
                    {this.state.citiesFilter.map((city)=> {
                        return <li>{city.name} {city.country}</li>
                    })}
                </ul>
            </div>
        )
    }
}
