import React, { Component } from 'react'
import Navbar from "react-bootstrap/Navbar"
import UserButton from "../src/components/UserButton"
import MenuButton from "../src/components/MenuButton"
import Homebutton from "../src/components/HomeButton"
import Paris from "../Images/Paris.jpg"


export default class CityFilter extends Component {
    constructor(props){
        super(props)
        this.state = {
            cities: [],
            citiesFilter: []}
    }

    filterCities = (e) => {
        let filteredCities = this.state.cities        
        filteredCities = filteredCities.filter((city) => {
          let cityFiltered = city.name.toLowerCase() + "  " + city.country.toLowerCase()
          return cityFiltered.indexOf(
            e.target.value.toLowerCase()) !== -1
        });
        this.setState({
          citiesFilter: filteredCities
        })
        
      }

    async componentDidMount(){
        var citiesResponse = await fetch("http://localhost:5000/api/cities")
        .then((data) => data.json());
        console.log(citiesResponse)
        this.setState({citiesFilter: citiesResponse,
        cities: citiesResponse})
    }

        
    render() {

        return (
            <div>
                <Navbar className="main-nav justify-content-between">
                <UserButton />
                <MenuButton />
              </Navbar>
                <label htmlFor="filter">Filter by city: </label>
                <input type="text" id="filter"  
                onChange={this.filterCities.bind(this)}/>
                <ul className="citiesImages">
                    {this.state.citiesFilter.map((city)=> {
                        return <li id={city.name}>
                            <div className="city">
                                <span>
                                {city.name}
                                </span>
                                <img src={Paris}></img>
                            </div>
                        </li>
                    })}
                </ul>
                <footer>
                    <Homebutton />
                </footer>
            </div>
        )
    }
}
