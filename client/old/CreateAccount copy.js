import React, { Component } from 'react';
import create from './Actions/createAccount';
import BackButtons from './BackButtons';
import TopLinks from './Top';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import $ from 'jquery'
import Axios from "axios"
import { Form } from 'react-bootstrap';

const mapDispatchToProps = dispatch => {
  return {
    create: data => dispatch(create(data))
  };
};

class CreateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picture: null,
      username: '',
      password: '',
      email: '',
      firstName: '',
      lastName: '',
      country: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(e) {
    const disableOk = () => { $("#create-button").addClass("disabled") }
    const enableOk = () => { $("#create-button").removeClass("disabled") }
    let inputFields = Object.entries(this.state)

    if (e.target.files){
      
      if (e.target.files[0] === undefined || null){return}

      let isAnImage = e.target.files[0]["type"].includes("image")
      if(isAnImage){
        $('#preview-img').attr("src", URL.createObjectURL(e.target.files[0]))
        this.setState({
          // picture: e.target.files[0]
          picture: e.target.files[0]
        })
      }
      console.log(this.state.picture)
    }

    this.setState({ [e.target.name]: e.target.value });
    for (let [key, value] of inputFields){
      if (key !== "username" || key !== "username"){
      value === '' ? disableOk() : enableOk()
      }
    }
  }

  async onSubmit(e) {
    e.preventDefault();
      // let {username, password, email, firstName, lastName, country} = this.state

      // var user = {
      //   username,
      //   password,
      //   email,
      //   firstName,
      //   lastName,
      //   country,
      //   favorites: []
      // };
      // console.log('creating');
      // this.props.create(user);
      const data = new FormData()
      data.append('picture', this.state.picture)

    data.get('picture')

      const image = await Axios.post('http://localhost:5000/api/users/image', data)
      .then(response => {console.log(response)})
      // .then(resp => resp.data);

    
  }

  componentDidMount() {
    // jquery to update file on click of "add photo"
    $('#add-image').click(function () {
      $('#picture').trigger('click')
    })

  }

  render() {
    const options = ["England", "France", "Germany", "Holland", "Ireland", "Spain", "United States"]
    return (
      <div id='create-new-account'>
        <TopLinks />
        <b id="title"><p>Create Account</p></b>

        <div id="add-image">
          <p>Add Photo</p>
          <img id="preview-img"></img>
        </div>

        <form>
          <input type="file" onChange={this.handleChange} name="picture" id="picture" />

          <div>
            <label for="username" >Username:</label>
            <input
              placeholder="optional"
              type='text'
              id='username'
              value={this.state.username}
              name='username'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="password">Password:</label>
            <input
              type='password'
              value={this.state.password}
              name='password'
              id="password"
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="email">Email:</label>
            <input
              type='text'
              value={this.state.email}
              name='email'
              id='email'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="first-name">First name:</label>
            <input
              type='text'
              value={this.state.firstName}
              name='firstName'
              id='first-name'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="last-name">Last name:</label>
            <input
              type='text'
              value={this.state.lastName}
              name='lastName'
              id='last-name'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="country">Country:</label>
            <select
              type='text'
              value={this.state.country}
              name='country'
              id='country'
              onChange={this.handleChange}
            >
              {options.map( (country) => {
                return <option value={country}>{country}</option>
              })}
            </select>
          </div>

          <Button className="disabled" color="primary" onClick={this.onSubmit} id="create-button">Ok</Button>

        </form>
        <BackButtons />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(CreateAccount);
