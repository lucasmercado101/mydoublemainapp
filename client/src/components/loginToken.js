import React, { Component } from 'react'
import logInAccount from './Actions/login';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => {
    return {
        logInAccount: token => dispatch(logInAccount(token))
    };
};

class loginToken extends Component {

    async componentDidMount() {
        let token = this.props.match.params.token
        await this.props.logInAccount(token)
        this.props.history.push('/cities')
    }

    render() {
        return (
            <div>

            </div>
        )
    }
}

export default connect(null, mapDispatchToProps)(loginToken);
