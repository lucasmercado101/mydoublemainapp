import React, { Component } from 'react';
import logInAccount from './Actions/login';
import { connect } from 'react-redux';
import TopLinks from './Top';
import { Button } from 'reactstrap';
import BackButtons from './BackButtons';
import { Link } from 'react-router-dom';
import $ from 'jquery'

const mapDispatchToProps = dispatch => {
  return {
    logInAccount: data => dispatch(logInAccount(data))
  };
};

const mapStateToProps = state => {
  return {
    user: state.userData.userObject
  };
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: localStorage.getItem('rem') || '',
      password: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.rememberButton = this.rememberButton.bind(this)
  }

  handleChange(e) {
    const disableOk = () => { $("#login-button").addClass("disabled") }
    const enableOk = () => { $("#login-button").removeClass("disabled") }
    let inputFieldsValues = Object.values(this.state)

    this.setState({ [e.target.name]: e.target.value });
    for (let input of inputFieldsValues) {
      input === '' ? disableOk() : enableOk()
    }
  }

  componentDidMount() {
    document.title = "Log in"
    let rememberMe = localStorage.getItem('rem')
    if (rememberMe) {
      $('#remember').prop('checked', true)
    }
  }

  rememberButton(e) {
    if ($('#remember').is(':checked')) {
      localStorage.setItem('rem', this.state.email);
    } else {
      localStorage.setItem('rem', "");
    }
  }

  async onSubmit(e) {
    // return <Redirect to='/cities' />
    if ($('#login-button').hasClass('disabled')) { return }
    if ($('#remember').is(':checked')) {
      localStorage.setItem('rem', this.state.email);
    }
    e.preventDefault();
    var user = {
      email: this.state.email,
      password: this.state.password
    };
    console.log('logging in');
    await this.props.logInAccount(user);
    this.props.history.push('/cities')
  }

  render() {
    return (
      <div id="login">
        <TopLinks />
        <div id="form">
          <p>Login</p>
          <form>
            <div className="email">
              <label for='email'>Email:</label>
              <input
                type='text'
                value={this.state.email}
                name='email'
                id="email"
                onChange={this.handleChange}
              />
            </div>

            <div>
              <label for="pass">Password:</label>
              <div className="empty"></div>
              <input
                type='password'
                value={this.state.password}
                name='password'
                id="pass"
                onChange={this.handleChange}
              />
            </div>

            <div id="remember-me">
              <label for="remember">Remember me</label>
              <input type="checkbox" name="rememberMe" onChange={this.rememberButton} value="remember" id="remember" />
            </div>
            <Button className="disabled" color="primary" onClick={this.onSubmit} id="login-button">Ok</Button>
          </form>
        </div>

        <div id="other-login">
          <Button color="info" id="google" href='http://localhost:5000/api/auth/google' style={{ borderRadius: "5px" }}>
            <div>
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Font_Awesome_5_brands_google-plus-g.svg/1279px-Font_Awesome_5_brands_google-plus-g.svg.png" alt="" />
              <p>Log in with Google</p>
            </div>
          </Button>
        </div>
        <footer>
          <b><p>Don't have a MYtinerary account yet?<br />
            You should create one It's totally free <br />
            and it only takes a minute.</p></b>
          <Link to={'/createAccount'}>
            <b><p id="create">Create Account</p></b>
          </Link>
          <BackButtons />
        </footer>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
