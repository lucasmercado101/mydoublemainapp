import React, { Component } from 'react';
import { connect } from 'react-redux';
import getComms from './Actions/getCommentsAction';
import { Button } from 'reactstrap';
import axios from 'axios'
import $ from 'jquery'

const mapStateToProps = state => {
    return {
        comments: state.comments.commentObject,
        user: state.userData.userObject
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getComms: city => dispatch(getComms(city))
    };
};

class comment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            commentObj: this.props.comment,
            index: this.props.index,
            edit: this.props.comment.comment
        }
        this.delete = this.delete.bind(this)
        this.edit = this.edit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        this.setState({
            edit: e.target.value
        })
    }

    async delete(e) {
        e.preventDefault()
        await axios.delete("http://localhost:5000/api/itineraries/comments/" + this.props.city, { data: this.props.comment })
        this.props.reloadComments()
    }

    edit(e) {
        e.preventDefault()
        let i = this.state.index
        $('.content' + i).hide()
        $('.del-btn' + i).hide()
        $('.edit-btn' + i).hide()
        $('.edit' + i).show()
    }

    componentDidMount() {
        let i = this.state.index
        console.log(this.props.comment)

        const finishEditing = () => {
            $('.edit' + i).hide()
            $('.content' + i).show()
            $('.del-btn' + i).show()
            $('.edit-btn' + i).show()
        }

        $('.confirm-edit' + i).click(async () => {
            let comment = { original: this.props.comment, edit: this.state.edit }
            await axios.put("http://localhost:5000/api/itineraries/comments/" + this.props.city, { data: comment })
            finishEditing()

            this.props.reloadComments()
        })
        $('.cancel-edit' + i).click(() => {
            finishEditing()
            this.setState({
                edit: this.props.comment.comment
            })
        })
    }

    render() { //put this in itineraries not city
        let canEdit = this.props.user.username === this.state.commentObj.user
        const i = this.state.index
        return (
            <div className="comment">
                {canEdit ?
                    <div data-index={this.state.index}>
                        <Button onClick={this.edit} className={"edit-btn" + i}>Edit</Button>
                        <Button onClick={this.delete} className={"del-btn" + i}>Delete</Button>
                    </div>
                    : ""}
                <p className="username">{this.state.commentObj.user}</p>
                <div className={"edit" + i + "  edit-style"} style={{ display: "none" }}>
                    <span className={"glyphicon glyphicon-remove cancel-edit" + i}></span>
                    <input type="text" onChange={this.handleChange} value={this.state.edit}></input>
                    <span className={"post-comment glyphicon glyphicon-triangle-right confirm-edit" + i}></span>
                </div>
                <p className={"content" + i}>{this.state.edit}</p>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(comment);
