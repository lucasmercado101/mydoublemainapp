import React, { Component } from 'react';
import allCities from './Actions/actionsCity';
import cityData from './Actions/itineraryActions';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import BottomLinks from './BackButtons';
import TopBar from './Top'
import $ from 'jquery'

const mapDispatchToProps = dispatch => {
  return {
    allCities: () => dispatch(allCities()),
    cityData: data => dispatch(cityData(data))
  };
};

const mapStateToProps = state => {
  return {
    cities: state.allCities.cities,
    user: state.userData.userObject
  };
};

class CityFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      citiesFilter: []
    };
  }

  filterCities = e => {
    let filteredCities = this.props.cities;
    filteredCities = filteredCities.filter(city => {
      let cityFiltered =
        city.name.toLowerCase() + '  ' + city.country.toLowerCase();
      return cityFiltered.indexOf(e.target.value.toLowerCase()) !== -1;
    });
    filteredCities.sort((a, b) => a.name.localeCompare(b.name));
    this.setState({
      citiesFilter: filteredCities
    });
    console.log(this.state.citiesFilter);
  };

  async componentDidMount() {
    document.title = "Cities"
    if (Object.entries(this.props.user).length === 0) {
      console.log("Log in!")
      return
    }
    console.log(localStorage.getItem('token'));
    console.log('before');
    await this.props.allCities();
    console.log('after');
    console.log(this.props.cities);
    if (this.props.cities === undefined) {

    } else {
      this.props.cities.sort((a, b) => a.name.localeCompare(b.name));
      await this.setState({
        citiesFilter: this.props.cities
      });
    }
    console.log(this.props);
    console.log("CitiesFilter es: ", this.state.citiesFilter)
    $('.on-cities').hide()
  }

  render() {
    return (
      <div id='city-outer'>
        <TopBar />
        {Object.entries(this.props.user).length === 0
          ?
          <div >
            <p><Link id="logged-out" to="/login">Log in</Link> to see this!</p>
          </div>
          :
          <div id="all-cities">
            <h1>CITIES</h1>
            <label htmlFor='filter'>Filter by city: </label>
            <input
              type='text'
              id='filter'
              onChange={this.filterCities.bind(this)}
            />
            <div className="lds-ring on-cities"><div></div><div></div><div></div><div></div></div>
          </div>

        }
        <ul>
          {this.state.citiesFilter.map(city => {
            return (
              <li>
                <Link to={'/cities/' + city.name}>
                  <span>{city.name}</span>
                  <img src={city.url} alt='pitcher'></img>
                </Link>
              </li>
            );
          })}
        </ul>
        <BottomLinks />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CityFilter);
