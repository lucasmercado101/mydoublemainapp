import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap';
import logOut from './Actions/logOut'
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    user: state.userData.userObject
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logOut: () => dispatch(logOut()),
  };
};

const TopLinks = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle id="user-button">
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem id="logout-button" onClick={() => {
          props.logOut();
          window.location.reload();
        }} >
          <p>Log out</p>
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(TopLinks);