import React, { Component } from 'react';
import { connect } from 'react-redux';
import $ from 'jquery';
import Axios from 'axios';
import logInAccount from './Actions/login';

const mapDispatchToProps = dispatch => {
  return {
    logInAccount: data => dispatch(logInAccount(data))
  };
};

const mapStateToProps = state => {
  console.log(state);
  return {
    user: state.userData.userObject
  };
};

export class Like extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user
    };
  }

  async componentDidMount() {
    let cityName = this.props.city; //city name passed from parent comp
    let userData = this.state.user;
    console.log(this.state.user)
    if (!userData.favorites) { //if user isn't logged in, do this or it breaks
      $('.like').css('color', 'blue')
      return
    }
    let userAlreadyLikedThisCity = userData.favorites.includes(this.props.city)
    console.log(userData.favorites)

    function debounce(func, wait, immediate) { //do function after wait time
      var timeout;
      return function () {
        var context = this, args = arguments;
        var later = function () {
          timeout = null;

          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    };

    const likeIt = async function () {
      await Axios.post('http://localhost:5000/api/users/favorites', {
        cityName,
        userData
      });
      var user = {
        email: this.state.user.email,
        password: this.state.user.password
      };
      this.props.logInAccount(user)
    }.bind(this);

    const dislikeIt = async function () {
      await Axios.delete('http://localhost:5000/api/users/favorites', {
        data:
        {
          cityName,
          userData
        }
      });
      var user = {
        email: this.state.user.email,
        password: this.state.user.password
      };
      this.props.logInAccount(user)
    }.bind(this);

    if (userAlreadyLikedThisCity) {
      $('.like').addClass("liked")
    };

    var fav = debounce(function () { likeIt() }, 1500);
    var unfav = debounce(function () { dislikeIt() }, 1500);

    $('.like').click(function () {
      $(this).toggleClass("liked")
      var justLiked = $(this).hasClass("liked")
      justLiked ? fav() : unfav()
    });
  }

  render() {
    return (
      <div>
        <span className='like'>&hearts;</span>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Like);
