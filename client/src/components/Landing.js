import React, { Component } from 'react'
import MYtineraryLogo from '../Images/MYtineraryLogo.png'
import CircledRight from '../Images/circled-right-2.png'
import ImagesCarousel from "./CarouselMain"
import TopLinks from './Top'
import { Link } from 'react-router-dom'

export default class Landing extends Component {

    render() {
        return (
            <div className="landing" id="landing">
                <TopLinks />
                <div className="Logo">
                    <img src={MYtineraryLogo} alt="Mytinerary Logo"></img>
                </div>
                <p>Find your perfect trip, designed by insiders who know and love their cities.</p>
                <div className="BrowsingArrow ">
                    <Link to={"/cities"}>
                        <img src={CircledRight} alt="Mytinerary Logo"></img>
                    </Link>
                </div>
                <div>
                    <p>Popular MYtineraries</p>
                    <ImagesCarousel />
                </div>
            </div>

        )

    }
}
