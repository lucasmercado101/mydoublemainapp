import React, { Component } from 'react'
import Comment from './comment';
import $ from 'jquery'

export default class CommentSection extends Component {
    constructor(props) {
        super(props)

        this.state = {
            commentToAdd: ""
        }
        this.handleChange = this.handleChange.bind(this)
        this.postComment = this.postComment.bind(this)
    }

    handleChange(e) {
        this.setState({
            commentToAdd: e.target.value
        })
    }

    postComment(e) {
        e.preventDefault()
        console.log("yes")
    }

    componentDidMount() {
    }

    render() {
        return (
            <div className="comment-section">
                <p>Comments: </p>
                <div className="all-comments">
                    <div class="lds-ring on-comment"><div></div><div></div><div></div><div></div></div>
                    {/* {this.state.allcomments.map((obj, i) => {
                        return <Comment reloadComments={this.reloadComments} index={i} city={this.props.match.params.cityName} comment={obj} />
                    })} */}
                </div>
                <div className="new-comment" >
                    <input
                        type="text" id="add-comment"
                        value={this.state.commentToAdd}
                        onChange={this.handleChange}
                        placeholder="Your comment..."
                    />
                    <button onClick={this.postComment}>
                        <span className="glyphicon glyphicon-triangle-right"></span>
                    </button>
                </div>
            </div>
        )
    }
}


