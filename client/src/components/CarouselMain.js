import React, { useState } from 'react';
import {Container, Row, Col} from  'reactstrap'


import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';

{/*}
const items = [
  { grupo:
    [{src: require('../Images/newyork.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/Paris.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/barcelona.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/amsterdam.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  },
  { grupo:
    [{src: require('../Images/newyork.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/Paris.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/barcelona.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/amsterdam.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  },
  { grupo:
    [{src: require('../Images/newyork.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/Paris.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/barcelona.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: require('../Images/amsterdam.jpg'),
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  }
];
*/}

const items = [
  { grupo:
    [{src: 'https://source.unsplash.com/featured/?city, a',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, b',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, c',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, d',
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  },
  { grupo:
    [{src: 'https://source.unsplash.com/featured/?city, e',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, f',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, g',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, h',
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  },
  { grupo:
    [{src: 'https://source.unsplash.com/featured/?city, i',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, j',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, l',
    altText: 'Slide 1',
    caption: 'Slide 1'},
    {src: 'https://source.unsplash.com/featured/?city, m',
    altText: 'Slide 1',
    caption: 'Slide 1'}
  ]
  }
];

const Example = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
          <Container>
                <Row>
          {item.grupo.map((i) => {
              return (
                <Col xs={{ size : 6 }} style={{ maxHeight: '10rem'}}>
            <img src={i.src} key = {i.src} alt={i.altText} style={{ height: '14rem', width: '17.5em'}} />
            </Col>
              )
          })}
          </Row>
        </Container>
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators  items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default Example;
