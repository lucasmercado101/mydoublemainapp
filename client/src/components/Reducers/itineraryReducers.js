const initialState = {
    currentCityData: [],
    activities: []
}

export default (state = initialState, action) => {
    console.log(action)
    switch(action.type){
        case 'GET_ITINERARY':
            return {
                ...state,
                currentCityData: action.payload
            }
        case 'GET_ACTIVITIES':
            return {
                ...state,
                activities: action.payload
            }
        default:
            return state
    }
}