const initialState = {
    commentObject: []
}

export default (state = initialState, action) => {
    console.log(action)
    switch (action.type) {
        case 'GET_COMMENTS':
            return {
                ...state,
                commentObject: action.payload
            }
        default:
            return state
    }
}