
const initialState = {
  userObject: {}
};

export default (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case 'LOGIN':
      // document.getElementById("user-button").style.backgroundImage = "url(" + action.payload.picture + ")"
      // document.getElementById("user-button").style.backgroundSize = "cover"
      // document.getElementById("user-button").style.borderRadius = "50%"
      console.log(action.payload.picture)
      return {
        ...state,
        userObject: action.payload
      };
    case 'LOGOUT':
      localStorage.setItem('token', '');
      return {
        ...state,
        userObject: {}
      };
    default:
      return state;
  }
};
