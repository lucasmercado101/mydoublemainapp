const initialState = {
    cities: []
}

export default (state = initialState, action) => {
    console.log(action)
    switch(action.type){
        case 'GET_CITIES':
            return {
                ...state,
                cities: action.payload
            }
        default:
            return state
    }
}