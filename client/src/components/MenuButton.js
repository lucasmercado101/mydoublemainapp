import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom'

const Example = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle id="menu-button">
        <span className="glyphicon glyphicon-align-justify"></span>
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem>
          <Link to={'/cities'}>
            Cities
        </Link>
        </DropdownItem>
        {/* <DropdownItem>
          <Link to={'/blah'}>
            Lorem ipsum
        </Link></DropdownItem> */}
      </DropdownMenu>
    </Dropdown>
  );
}

export default Example;