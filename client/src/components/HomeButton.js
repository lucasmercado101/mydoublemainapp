import React from 'react';
import { Link } from 'react-router-dom';

const Example = (props) => {
  return (
    <div id="home-button">
      
      <Link to={'/'}>
        <span className="glyphicon glyphicon glyphicon-home"></span>
        </Link>
    </div>
  );
}

export default Example