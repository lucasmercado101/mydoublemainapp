import React, { Component } from 'react';
import create from './Actions/createAccount';
import BackButtons from './BackButtons';
import TopLinks from './Top';
import { Button } from 'reactstrap';
import logInAccount from './Actions/login';
import { connect } from 'react-redux';
import $ from 'jquery'
import Axios from "axios"

const mapDispatchToProps = dispatch => {
  return {
    logInAccount: data => dispatch(logInAccount(data)),
    create: data => dispatch(create(data))
  };
};

class CreateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picture: '',
      username: '',
      password: '',
      email: '',
      firstName: '',
      lastName: '',
      country: "England"
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(e) {
    const disableOk = () => { $("#create-button").addClass("disabled") }
    const enableOk = () => { $("#create-button").removeClass("disabled") }
    let inputFields = Object.entries(this.state)

    $('#preview-img').attr("src", this.state.picture)

    this.setState({ [e.target.name]: e.target.value });
    for (let [key, value] of inputFields) {
      if (key !== "username") {
        if (value) { enableOk() }
        else {
          disableOk()
          return
        }
      }
    }
  }

  async onSubmit(e) {
    e.preventDefault();
    if ($('#create-button').hasClass('disabled')) { return }

    let { username, password, email, firstName, lastName, country, picture } = this.state

    if (username === "") { username = firstName + " " + lastName }

    var user = {
      username,
      password,
      email,
      firstName,
      lastName,
      country,
      picture,
      favorites: []
    };
    try {
      await Axios.post('http://localhost:5000/api/users', user)
        .then(response => { console.log(response) })
    } catch (error) {
      alert("The email entered already has an account!")
    }
    finally {
      let login = {
        email,
        password
      };
      $('#title').hide()
      $('#create-account-form').hide()
      $('.lds-ring').show().addClass('on-create')

      await this.props.logInAccount(login)
      this.props.history.push('/cities')
    }
  }

  componentDidMount() {
    // jquery to update file on click of "add photo"
    $('#add-image').click(function () {
      $('#picture').trigger('click')
    })

  }

  render() {
    const options = ["England", "France", "Germany", "Holland", "Ireland", "Spain", "United States"]
    return (
      <div id='create-new-account'>
        <TopLinks />
        <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
        <b id="title"><p>Create Account</p></b>

        <form id="create-account-form">

          <label for="picture">Image URL:</label>
          <input type="url" name="picture" id="picture"
            placeholder="https://example.com"
            value={this.state.picture}
            onChange={this.handleChange}
            pattern="https://.*" size="30"
            required></input>

          {/* <div id="add-image">
            <img id="preview-img" src=""></img>
          </div> */}

          <div>
            <label for="username" >Username:</label>
            <input
              placeholder="optional"
              type='text'
              id='username'
              value={this.state.username}
              name='username'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="password">Password:</label>
            <input
              type='password'
              value={this.state.password}
              name='password'
              id="password"
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="email">Email:</label>
            <input
              type='text'
              value={this.state.email}
              name='email'
              id='email'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="first-name">First name:</label>
            <input
              type='text'
              value={this.state.firstName}
              name='firstName'
              id='first-name'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="last-name">Last name:</label>
            <input
              type='text'
              value={this.state.lastName}
              name='lastName'
              id='last-name'
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label for="country">Country:</label>
            <select
              type='text'
              value={this.state.country}
              name='country'
              id='country'
              onChange={this.handleChange}
            >
              {options.map((country) => {
                return <option value={country}>{country}</option>
              })}
            </select>
          </div>

          <Button className="disabled" color="primary" onClick={this.onSubmit} id="create-button">Ok</Button>

        </form>
        <BackButtons />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(CreateAccount);
