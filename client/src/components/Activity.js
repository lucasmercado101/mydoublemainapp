import React, { Component } from 'react'
import { Card, Button, Accordion } from 'react-bootstrap'
import { connect } from 'react-redux'
import activityData from './Actions/itineraryActivitiesActions'
import gl from '../Images/GaudiLover.png'
import ha from '../Images/HambreAlumna.png'
import CommentSection from './CommentSection'


const mapStateToProps = (state) => {
    console.log(state)
    return {
        activitiesData: state.itineraries.activities
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        activityData: () => dispatch(activityData())
    }
}

class Activity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activities: []
        }
    }

    async componentDidMount() {
        await this.props.activityData()
        this.setState({
            activities: this.props.activitiesData
        })
        console.log(this.props.activitiesData)
        console.log(this.state.activities)
    }

    render() {
        return (
            <div>
                <Accordion>
                    <Card className="card-itinerary">
                        <Card.Body className="body">
                            <Card.Img className="img" src={gl} alt="profile pic" />
                            <div>
                                <Card.Title className="title">Gaudi In A Day</Card.Title>
                                <div className="description">
                                    <Card.Text className="text-push">
                                        Likes: 34
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        12 Hours
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        $$
                                </Card.Text>
                                </div>
                                <div className="hashtags">
                                    <Card.Text className="text-push">
                                        #Art
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        #Architecture
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        #History
                                </Card.Text>
                                </div>
                            </div>
                        </Card.Body>
                        <Card.Footer className="more-button">
                            <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                View All
                        </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Card.Text className="text">
                                        Activities
                                    </Card.Text>
                                    <ul>
                                        {this.state.activities.map((activity) => {
                                            if (activity._id === "5dce949b1c9d440000d551cb") {
                                                return activity.activities.map((activityData) => {
                                                    return <li>{activityData}</li>
                                                })
                                            }
                                        })}
                                    </ul>
                                    <CommentSection />
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card.Footer>
                    </Card>
                </Accordion>

                <Accordion>
                    <Card className="card-itinerary">
                        <Card.Body className="body">
                            <Card.Img className="img" src={ha} alt="profile pic" />
                            <div>
                                <Card.Title className="title">Tapa Till You Droppa</Card.Title>
                                <div className="description">
                                    <Card.Text className="text-push">
                                        Likes: 24
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        4 Hours
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        $$
                                </Card.Text>
                                </div>
                                <div className="hashtags">
                                    <Card.Text className="text-push">
                                        #Restaurants
                                </Card.Text>
                                    <Card.Text className="text-push">
                                        #Food&Drink
                                </Card.Text>
                                </div>
                            </div>
                        </Card.Body>
                        <Card.Footer className="more-button">
                            <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                View All
                        </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Card.Text className="text">
                                        Activities
                                </Card.Text>
                                    <ul>
                                        {this.state.activities.map((activity) => {
                                            if (activity._id === "5dce94ef1c9d440000d551cd") {
                                                return activity.activities.map((activityData) => {
                                                    return <li>{activityData}</li>
                                                })
                                            }
                                        })}
                                    </ul>
                                    <CommentSection />
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card.Footer>
                    </Card>
                </Accordion>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Activity)