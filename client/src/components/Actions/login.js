import Axios from 'axios';
import jwt_decode from 'jwt-decode';

const logInAccount = data => async dispatch => {
  if (typeof data === 'string') { //google login
    let userObj = jwt_decode(data);
    data = {
      email: userObj.email,
      password: userObj.password
    }
  }
  const response = await Axios.post(
    'http://localhost:5000/api/users/login',
    data
  ).then(resp => resp.data);
  console.log(response);
  // response is token
  localStorage.setItem('token', response.token);
  let decoded_token = jwt_decode(response.token);
  dispatch({
    type: 'LOGIN',
    payload: decoded_token
  });
};

export default logInAccount;
