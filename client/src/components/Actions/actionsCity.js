import axios from 'axios';

const allCities = () => async dispatch => {
  const response = await axios
    .get('http://localhost:5000/api/cities', {
      headers: {
        Authorization: 'bearer ' + localStorage.getItem('token')
      }
    })
    .then(resp => resp.data)
    .catch(err => console.log(err));
  console.log(response);
  dispatch({
    type: 'GET_CITIES',
    payload: response
  });
};

export default allCities;
