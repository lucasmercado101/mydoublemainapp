import Axios from 'axios';

const cityData = data => async dispatch => {
  console.log('getting city data.');
  const response = await Axios.get(
    'http://localhost:5000/api/itineraries/' + data
  )
    .then(resp => resp.data)
    .catch(err => err.data);
  console.log(response);
  dispatch({
    type: 'GET_ITINERARY',
    payload: response
  });
};

export default cityData;
