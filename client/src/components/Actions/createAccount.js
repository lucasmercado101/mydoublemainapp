import Axios from "axios";

const postAccount = (data) => async (dispatch) => {
    const response = await Axios.post('http://localhost:5000/api/users', data)
    .then(resp => resp.data);
    console.log(response)
    dispatch (
         {
            type: 'CREATE_ACCOUNT',
            payload: response
        }
    )
}

export default postAccount