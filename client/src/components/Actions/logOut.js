const logOutAccount = data => async dispatch => {
  dispatch({
    type: 'LOGOUT',
    payload: ''
  });
};

export default logOutAccount;
