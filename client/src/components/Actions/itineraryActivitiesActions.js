import axios from 'axios'
const activityData = () => async (dispatch) => {
    const response = await axios.get("http://localhost:5000/api/cities/activities").then(resp => resp.data);
    console.log(response)
    dispatch (
         {
            type: 'GET_ACTIVITIES',
            payload: response
        }
    )
} 

export default activityData