import axios from 'axios'
const getComments = (city) => async (dispatch) => {
    var response = await axios.get("http://localhost:5000/api/itineraries/comments/" + city).then(resp => resp.data);
    console.log(response)
    dispatch(
        {
            type: 'GET_COMMENTS',
            payload: response
        }
    )
}

export default getComments