import React, { useState } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    user: state.userData.userObject
  };
};

const TopLinks = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle id="user-button">
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem>
          <Link to={'/Login'}>
            Log in
        </Link>
        </DropdownItem>
        <DropdownItem>
          <Link to={'/createAccount'}>
            Create Account
        </Link></DropdownItem>
      </DropdownMenu>
    </Dropdown>
  );
}

export default connect(mapStateToProps, null)(TopLinks);