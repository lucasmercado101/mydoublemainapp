import React, { Component } from 'react'
import Homebutton from './HomeButton'
import { Link } from 'react-router-dom';

export default class BackButtons extends Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        console.log(this.props.to)
    }
    render() {
        return (
            <div id="back-buttons">
                <Homebutton />
                <div id="back">
                    <Link to={this.props.to != null ? this.props.to : "/"}>
                        <span className='glyphicon glyphicon-triangle-left'></span>
                    </Link>
                </div>
            </div>
        )
    }
}
