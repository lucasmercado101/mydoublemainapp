import React, { Component } from 'react';
import cityData from './Actions/itineraryActions';
import getComms from './Actions/getCommentsAction';
import allCities from './Actions/actionsCity';
import { connect } from 'react-redux';
import Activity from './Activity';
import { Link } from 'react-router-dom';
import LikeButton from './like';
import BottomLinks from './BackButtons';

const mapStateToProps = state => {
  console.log(state);
  return {
    user: state.userData.userObject,
    cities: state.allCities.cities,
    comments: state.comments.commentObject,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    cityData: data => dispatch(cityData(data)),
    allCities: () => dispatch(allCities()),
    getComms: city => dispatch(getComms(city)),
  };
};

class Itinerary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cityData: [],
      activities: [],
      commentObject: [],
      allcomments: [],
      commentToAdd: ""
    };
  }

  async componentDidMount() {
    await this.props.allCities()
    for (let i in this.props.cities) {
      // console.log(this.props.cities[i].name)
      if (this.props.cities[i].name == this.props.match.params.cityName) {
        console.log(this.props.cities[i])
        this.setState({
          cityData: [this.props.cities[i]],
        })
      }
    }
    document.title = this.props.match.params.cityName
    console.log(this.state.currentCity)
    console.log("user stuff: ", this.props.user)
  }

  render() {
    return (
      <div
        id='city-itineraries'
        style={{ display: 'flex', flexDirection: 'column' }}
      >
        {this.state.cityData.map(city => {
          return (
            <div id='city-data'>
              <header id='header-image'>
                <span id='city-title'>{city.name}</span>
                <img
                  src={city.url}
                  alt={city.name}
                ></img>
              </header>
            </div>
          );
        })}
        <p id='available-text' style={{ margin: '10px 10px' }}>
          Available Itineraries:
        </p>
        <Activity />
        {this.state.cityData.map(city => {
          return <LikeButton city={city.name} />;
        })}

        <footer>
          <Link to={'/cities'} style={{ alignSelf: 'center' }}>
            <span id='another-city'>choose another city...</span>
          </Link>
          <BottomLinks to="/cities" />
        </footer>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Itinerary);
