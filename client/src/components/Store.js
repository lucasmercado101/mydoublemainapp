import { createStore, applyMiddleware, combineReducers } from 'redux'
import citiesReducer from './Reducers/reducerCities'
import thunk from 'redux-thunk'
import itineraryReducers from './Reducers/itineraryReducers'
import userReducer from './Reducers/userReducer';
import comments from './Reducers/comments';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

const reducers = combineReducers({
  allCities: citiesReducer,
  itineraries: itineraryReducers,
  userData: userReducer,
  comments
})

const store = createStore(
  reducers,
  composeEnhancer(applyMiddleware(thunk)),
);
export default store 