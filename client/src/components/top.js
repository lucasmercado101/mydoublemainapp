import React, { Component } from 'react'
import AnonButton from './loggedOutButton'
import LoggedInButton from './loggedInButton'
import Menu from './MenuButton'
import $ from 'jquery'
import defaultImage from '../Images/defaultProfile.png'
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        user: state.userData.userObject
    };
};

class top extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let pfp = this.props.user.picture || defaultImage
        $('#user-button').css({
            "background-image": `url( ${pfp} )`
        })
    }

    render() {
        return (
            <div id="top-bar">
                {this.props.user.picture ? <LoggedInButton /> : <AnonButton />}
                <Menu />
            </div>
        )
    }
}

export default connect(mapStateToProps, null)(top);
