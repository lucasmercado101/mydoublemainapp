import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Cities from './components/Cities';
import Landing from './components/Landing';
import LogIn from './components/LogIn';
import NotFound from './components/NotFound';
import Itinerary from './components/Itinerary';
import Token from './components/loginToken';
import CreateAccount from './components/CreateAccount';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './Styles/main.css';

function App() {
  return (
    <BrowserRouter>
      <div className='App'>
        <Switch>
          <Route exact path='/' component={Landing} />
          <Route exact path='/cities' component={Cities} />
          <Route exact path='/cities/:cityName' component={Itinerary} />
          <Route exact path='/login' component={LogIn} />
          <Route exact path='/logIn/token/:token' component={Token} />
          <Route exact path='/createAccount' component={CreateAccount} />
          <Route component={NotFound} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
